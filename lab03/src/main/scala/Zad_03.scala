package lab03
//@annotation.tailrec
def reverse(napis: String): String = {
    
    if (napis.isEmpty) {
        napis
    } else {
        reverse(napis.tail) + napis.head
    }
    // def pomocnicza(/*argumenty*/): String = {
    //     ???
    // }
}

@main def zad_03(napis: String): Unit = {
    //Napisz funkcje reverse, która dla podanego napisu zwraca odwrócony napis
    //Wykorzystaj operacje head i tail na napisach
    val odwrócony = reverse(napis)
    println(s"$napis od końca to $odwrócony")
}