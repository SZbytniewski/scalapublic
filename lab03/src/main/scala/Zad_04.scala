package lab03

def IntToBin(liczba: Int): String = {
    //@annotation.tailrec
    if (liczba <= 1) {
        liczba.toString
    } else {
        IntToBin(liczba / 2) + (liczba % 2).toString
  }
}

@main def zad_04(liczba: Int): Unit = {
    require(liczba>=0)
    //Napisz funkcję IntToBin, która dla podanej liczby naturalnej zwróci jej reprezentację w systemie binarnym
    val binarna = IntToBin(liczba)
    println(s"$liczba w systemie binarnym jest zapisywana jako $binarna")
}