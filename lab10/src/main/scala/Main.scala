package lab10

sealed trait Frm
case object False extends Frm
case object True extends Frm
case class Not(f: Frm) extends Frm {
  override def toString(): String = f match {
    case True => s"!$f"
    case False => s"!$f"
    case Not(f) => s"$f"
    case And(f1, f2) => s"!($f)"
    case Or(f1, f2) => s"!($f)"
    case Imp(f1, f2) => s"!($f)"
  }
}
case class And(f1: Frm, f2: Frm) extends Frm {
  override def toString(): String = (f1, f2) match {
    case (True, True) | (True, False) | (False, True) | (False, False) => s"$f1 & $f2"
    case (Not(f1), Not(f2)) => s"${Not(f1)} & ${Not(f2)}"
    case (True, Not(f2)) => s"$f1 & ${Not(f2)}"
    case (False, Not(f2)) => s"$f1 & ${Not(f2)}"
    case (Not(f1), True) => s"${Not(f1)} & $f2"
    case (Not(f1), False) => s"${Not(f1)} & $f2"
    case _ => s"($f1) & ($f2)"
  }
}
case class Or(f1: Frm,f2: Frm) extends Frm {
  override def toString(): String = (f1, f2) match {
      case (True, True) | (True, False) | (False, True) | (False, False) => s"$f1 | $f2"
      case (Not(f1), Not(f2)) => s"${Not(f1)} | ${Not(f2)}"
      case (True, Not(f2)) => s"$f1 | ${Not(f2)}"
      case (False, Not(f2)) => s"$f1 | ${Not(f2)}"
      case (Not(f1), True) => s"${Not(f1)} | $f2"
      case (Not(f1), False) => s"${Not(f1)} | $f2"
      case _ => s"($f1) | ($f2)"
  }
}
case class Imp(f1: Frm, f2: Frm) extends Frm {
  override def toString(): String = (f1, f2) match {
      case (True, True) | (True, False) | (False, True) | (False, False) => s"$f1 -> $f2"
      case (Not(f1), Not(f2)) => s"${Not(f1)} -> ${Not(f2)}"
      case (True, Not(f2)) => s"$f1 -> ${Not(f2)}"
      case (False, Not(f2)) => s"$f1 -> ${Not(f2)}"
      case (Not(f1), True) => s"${Not(f1)} -> $f2"
      case (Not(f1), False)=> s"${Not(f1)} -> $f2"
      case (_, True) | (_, False) => s"($f1) -> $f2"
      case (True, _) | (False, _) => s"$f1 -> ($f2)"
      case (Not(f1), _) => s"${Not(f1)} -> ($f2)"
      case (_, Not(f2)) => s"($f1) -> ${Not(f2)}"
      case _ => s"($f1) -> ($f2)"
  }
}

val frm = Imp(Or(True, False), Not(And(True, False)))
val frm2 = And(Imp(Not(And(True, False)), Not(False)), And(True, True))
val frm3 = Not(Not(And(True, False)))
val frm4: Frm = And(Not(Imp(False, True)), True)
val frm5: Frm = Imp(Not(And(True, False)), Not(True))
val frm6: Frm = Or(True, And(True, False))

// UWAGA: W rozwiązaniach poniższych zadań (tam gdzie to możliwe)
//        wykorzystaj rekurencję ogonową.

@main def zad_1: Unit = {
  // Ćwiczenie 1: zaimplementuj toString tak, aby „minimalizowało”
  //              liczbę nawiasów
  println(frm)
  println(frm2)
  // Powinno wyprodukować coś „w stylu”:
  // (True | False) -> !(True & False)
}

@main def zad_2: Unit = {
  // Ćwiczenie 2: zaimplementuj funkcję wyliczającą wartość logiczną
  //              formuły.
  // eval(False) == false
  // eval(frm) == true
  def subformulas(f: Frm): List[Frm] = {
      @annotation.tailrec
      def subformulasHelper(stack: List[Frm], acc: List[Frm] = Nil): List[Frm] = stack match {
        case Nil => acc
        case frm :: tail => frm match {
          case And(f1, f2) => subformulasHelper(f1 :: f2 :: tail, frm :: acc)
          case Or(f1, f2) => subformulasHelper(f1 :: f2 :: tail, frm :: acc)
          case Imp(f1, f2) => subformulasHelper(f1 :: f2 :: tail, frm :: acc)
          case Not(f) => subformulasHelper(f :: tail, frm :: acc)
          case _ => subformulasHelper(tail, frm :: acc)
        }
      }
      subformulasHelper(List(f))
    }
    println(subformulas(frm))
    println(subformulas(frm2))
    println(subformulas(frm3))
    println(subformulas(frm5))

  def eval(f: Frm): Boolean = {
    @annotation.tailrec
    def evalHelper(stack: List[Frm], acc: List[Boolean] = Nil): Boolean = stack match {
      case Nil => acc.head
      case frm :: rest => frm match {
        case And(f1, f2) => evalHelper(f2 :: f1 :: rest, (acc.head && acc.tail.head) :: acc.drop(2))
        case Or(f1, f2) => evalHelper(f2 :: f1 :: rest, (acc.head || acc.tail.head) :: acc.drop(2))
        case Imp(f1, f2) => evalHelper(f2 :: f1 :: rest, (!acc.head || acc.tail.head) :: acc.drop(2))
        case Not(f) => f match {
          case Not(subf) => evalHelper(rest, acc.head :: acc.tail)
          case _ => evalHelper(rest, !acc.head :: acc.tail)
        }
        case True => evalHelper(rest, true :: acc)
        case False => evalHelper(rest, false :: acc)
      }
    }
    evalHelper(subformulas(f))
  }
  println(eval(frm))
  println(eval(frm2))
  println(eval(Not(Not(And(True, False)))))
  println(eval(frm4))
  println(eval(frm5))
  println(eval(frm6))
}
  // Wersja bez rekurencji ogonowej
  // def eval(f: Frm): Boolean = {
  //   // funkcja dla danych formatu conjunctive normal form: (A lub B lub C) i (D lub E lub F)
  //   def evalHelper(f: Frm): Boolean =  f match {
  //     case True => true
  //     case False => false
  //     case Not(f) => !evalHelper(f)
  //     case And(f1, f2) => evalHelper(f1) && evalHelper(f2)
  //     case Or(f1, f