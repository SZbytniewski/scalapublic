package lab04

@annotation.tailrec
def ciąg(n: Int, a: Int = 2, b: Int = 1): Int = {
    if (n == 0) {
      a
    } else if (n == 1) {
      b
    } else {
      ciąg(n-1,b,a+b)
    }
}

@main def zadanie_01(liczba: Int): Unit = {
  println(ciąg(liczba))
  // Program powinien umożliwić „sprawdzenie” działania
  // funkcji „ciąg”.
}
