package lab04

def tasuj(l1: List[Int], l2: List[Int], l3: List[Int] = Nil): List[Int] = {
  def last(list: List[Int]): Int = {
      list match {
        case Nil => 0
        case head :: Nil => head
        case _ :: tail => last(tail)
      }
      
    }

    (l1,l2) match {
      case (head1 :: tail1, head2 :: tail2) if(head1 < head2) => tasuj(tail1,l2,l3 :+ head1)
      case (head1 :: tail1, head2 :: tail2) if(head1 > head2) => tasuj(l1,tail2,l3 :+ head2)
      case (head1 :: tail1, head2 :: tail2) if (head1 == head2 && last(l3) == head1) => tasuj(tail1,l2,l3)
      case (head1 :: tail1, head2 :: tail2) if (head1 == head2 && last(l3) != head1) => tasuj(tail1,l2,l3 :+ head1)
      case (head1 :: tail1, head2 :: tail2) if (head1 == head2 && last(l3) == head2) => tasuj(l1,tail2,l3)
      case (head1 :: tail1, head2 :: tail2) if (head1 == head2 && last(l3) != head2) => tasuj(l1,tail2,l3 :+ head2)
      case _ => l3
    }
    
  }

@main def zadanie_02: Unit = {
  // Program powinien umożliwić „sprawdzenie” działania
  // funkcji „tasuj”.
  val lista1 = List(2, 4, 3, 5)
  val lista2 = List(1, 2, 2, 3, 1, 5)
  val lista3 = List()
  val result = tasuj(lista1, lista2, lista3)
  println(result)
}
