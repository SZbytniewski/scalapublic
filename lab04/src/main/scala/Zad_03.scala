package lab04

def sumuj(l: List[Option[Double]], acc: Double = 0): Option[Double] = {
    l match {
      case (Some(head) :: tail) if (head > 0) => sumuj(tail,acc + head)
      case (Some(head) :: tail) if (head <= 0) => sumuj(tail,acc)
      case (_ :: tail) => sumuj(tail, acc)
      case _ => Some(acc)
    }
    
}

@main def zadanie_03: Unit = {
  // Program powinien umożliwić „sprawdzenie” działania
  // funkcji „sumuj”.
  val lista = List(Some(4.0), Some(-3.0), None, Some(1.0), Some(0.0))
  val result = sumuj(lista)
  println(result)
}
