import akka.actor.{ActorSystem, Actor, ActorLogging, ActorRef, Props}
import scala.collection.mutable.Queue

case class Oblicz(n: Int)

case class Wynik(n: Int, fib: BigInt)

class Boss extends Actor with ActorLogging {
  private def createNadzorca(nadzorcaId: Int): ActorRef = {
    context.actorOf(Props(new Nadzorca()), s"nadzorca-$nadzorcaId")
  }

  private def getNextNadzorcaId(currentId: Int): Int = {
    context.child(s"nadzorca-$currentId") match {
      case Some(_) => getNextNadzorcaId(currentId + 1)
      case None => currentId
    }
  }

  // Te funkcje tworza nam nadzorce i dajemy mu pewne id

  def receive: Receive = {
    case Oblicz(n) =>
      val nadzorcaId = getNextNadzorcaId(0)
      val nadzorca = createNadzorca(nadzorcaId)
      nadzorca ! Oblicz(n) // tworzymy nadzorce i wysylamy mu wiadomosc Oblicz

    case Wynik(n, value) => // Boss wypisuje wynik
      log.info(s"n = ${n.toString()}, result = ${value.toString()}")
  }
}

class Nadzorca(cache: Map[Int, BigInt] = Map(1 -> 1, 2 -> 1), doZrobienia: Set[Int] = Set())
  extends Actor with ActorLogging {

  def receive: Receive = {
    case Oblicz(k) =>
      val result = cache.get(k) //Sprawdza czy jest podany w mapie cache
      result match {
        case Some(value) => // Jesli jest to wyslyma do rodzinca wynik 
          context.parent ! Wynik(k, value) 
        case None => // Jak nie to tworzymy pracownika  zmieniamy recive na receive calcualted
                      // tak aby jak pracownik odesle to mial case i wysylamy wiadomosc
          val pracownik: ActorRef = context.actorOf(Props(classOf[Pracownik], k))
          context.become(receiveCalculated)
          pracownik ! Oblicz(k)
      }
  }

  def receiveCalculated: Receive = {
    case (key: Int, value: BigInt) => // Otrzymujemy wynik aktulizujemy cachc aby zapisał sobie wyniki na przyszlosc i 
                                      // I znowu zmieniamy receive na pierwszy aby kolejna wiadomosc mogla zostac odebrana
      val updatedCache = cache.updated(key, value)
      context.parent ! Wynik(key, updatedCache(key))
      context.become(receive) // Switch back to the original receive behavior
  }
}

class Pracownik(k: Int) extends Actor with ActorLogging {
  // Wyliczenie fibo
  def fibo(curr: Int, a: BigInt = 1, b: BigInt = 1): BigInt = {
    if (curr == 1) a
    else if (curr == 2) b
    else fibo(curr - 1, b, a + b)
  }

  def receive: Receive = {
    case Oblicz(k) => // Jak otrzymamy wiadomosc Oblicz to wylicza fibo i wysyla do rodzica swojego NIE GLOWNEGO KTORYM JEST BOSS
      val result = (k, fibo(k))
      context.parent ! result
  }
}

object ActorApp extends App {
  val system: ActorSystem = ActorSystem("MySystem")
  val boss: ActorRef = system.actorOf(Props[Boss](), "boss")

  //Wysyłamy kilka wiadomosci zeby zobaczyc czy dostaniemy wynik
  boss ! Oblicz(3)
  boss ! Oblicz(5)
  boss ! Oblicz(8)
  boss ! Oblicz(10)

}