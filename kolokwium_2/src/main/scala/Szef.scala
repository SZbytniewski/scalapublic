package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

abstract class DoSzefa
case class W(słowo: String) extends DoSzefa
case class I(słowo: String) extends DoSzefa
case class Ile(słowo: String, n: Int) extends DoSzefa

class Szef extends Actor with ActorLogging {
  def receive: Receive = {
    case W(slowo) =>
      val pracownikPierwszy = context.actorOf(Props[Pracownik]()) // incjalizacja 1 racownika 
      pracownikPierwszy ! Wstaw(slowo) //zaczyna na tym slowie pracowac 
      context.become(receive2(Map(slowo.head -> pracownikPierwszy))) // utworzenie mapy od tego momentu mamy juz pierwszy literke jako sciezke

    case I(slowo) => // jak chcemy cos znalesc jak jeszcze nawet nie ma nic 
      log.info(s"(${slowo}, 0)") 
  }

  def receive2(mapaPracownikow: Map[Char, ActorRef]): Receive = {
    case W(slowo) => //kazde inne slowo idzie ta droga
      val isPracownik = mapaPracownikow.find(x => x._1 == slowo.head) // sprawdzawmy czy jest juz w liscie ta lietka
      isPracownik match {
        case None => // jak nie to tworzymy nowa poczatkowa sciezke
          //log.info(s"${mapaPracownikow}")
          val pracownik = context.actorOf(Props[Pracownik]())
          pracownik ! Wstaw(slowo) // i wysylamy
          context.become(receive2(mapaPracownikow + (slowo.head -> pracownik))) // dodajemy do listy poczatkowej
        case Some(odbiorca) => // jak jest to wyslyamy pracownikowi temu to slowo
          odbiorca._2 ! Wstaw(slowo)
      }
    
    case I(slowo) => 
      val isPracownik = mapaPracownikow.find(x => x._1 == slowo.head) // sprawdza cyz jest takie slowo
      isPracownik match {
        case None => //jak nie ma to zwracamy odp
          log.info(s"(${slowo}, 0)")
        case Some(odbiorca) => //jak jest to zaczynamy sprawdzac dalej
          odbiorca._2 ! Policz(slowo, slowo, self) 
      }

    case Ile(slowo, licznik) => 
      log.info(s"(${slowo}, ${licznik})") // drukujemy wynik

 }
}
