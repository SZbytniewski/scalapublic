package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

abstract class DoPracownika
case class Wstaw(słowo: String) extends DoPracownika
case class Policz(slowo: String, slowo_tail: String, szef: ActorRef) extends DoPracownika

class Pracownik extends Actor with ActorLogging {
    def receive: Receive = {
      case Wstaw(slowo) =>
        if slowo.length == 0 then context.become(litera_na_koncu(Map(), 1)) //
        else {
          val nowy_pracownik = context.actorOf(Props[Pracownik]()) // tworzymy dziecko pracowinika kotre przyjumej taila tego slowa
          nowy_pracownik ! Wstaw(slowo.tail)
          context.become(litera_w_srodku(Map(slowo.head -> nowy_pracownik)))
        }
      
    }

    def litera_w_srodku(mapaPracownikow: Map[Char, ActorRef]): Receive = {
      case Wstaw(slowo) => // rozkladamy tak dlugo az dojedziemy do konca wtedy wysylamy do litery na koncu ktora trzyma licznik takich slow
        // log.info(s"dostałem ${slowo}")
        // log.info(s"$mapaPracownikow")
        if slowo.length == 0 then context.become(litera_na_koncu(mapaPracownikow, 1))
        else {  
          val isPracownik = mapaPracownikow.find(x => x._1 == slowo.head) // sprawdzamy czy jest
          isPracownik match {
            case None => // jak nie ma to tworzymy nowa sciezke
              val nowy_pracownik = context.actorOf(Props[Pracownik]())
              nowy_pracownik ! Wstaw(slowo.tail)
              context.become(litera_w_srodku(mapaPracownikow + (slowo.head -> nowy_pracownik)))
            case Some(odbiorca) => // jak jest to lecimy to samo sciezka narazie
              odbiorca._2 ! Wstaw(slowo.tail)
          }
        }
      case Policz(slowo, slowo_tail, szef) =>
        if slowo_tail.length == 0 then { // jak okaze sie ze nie ma to zwracamy 0
          szef ! Ile(slowo, 0)
        } 
        else { // inaczej lecimy literka po literce
          val isPracownik = mapaPracownikow.find(x => x._1 == slowo_tail.head)
          isPracownik match {
            case None => // jesli napotkamy w jakims momencie literke ktora nie ma sciezki to zwracamy 0 
              szef ! Ile(slowo, 0)
            case Some(odbiorca) => // jak jest to lecimy dalej az do literki na koncu 
              odbiorca._2 ! Policz(slowo, slowo_tail.tail, szef) 
          }
        }
    }

    def litera_na_koncu(mapaPracownikow: Map[Char, ActorRef], licznik: Int): Receive = {
      case Wstaw(slowo) =>
        // log.info(s"dostałem ${slowo}")
        if slowo.length == 0 then {
          context.become(litera_na_koncu(mapaPracownikow, licznik + 1)) // jak doszlismy do konca to dodajemy 1 do juz licznika zeby wiedziec ile kopi tego slowa mamy
        } else {  // inaczej lecimy dalej 
          val isPracownik = mapaPracownikow.find(x => x._1 == slowo.head)
          isPracownik match {
            case None =>
              val nowy_pracownik = context.actorOf(Props[Pracownik]())
              nowy_pracownik ! Wstaw(slowo.tail)
              context.become(litera_na_koncu(mapaPracownikow + (slowo.head -> nowy_pracownik), licznik))
            case Some(odbiorca) => 
              odbiorca._2 ! Wstaw(slowo.tail)
          }
        }

      case Policz(slowo, slowo_tail, szef) => // 
        if slowo_tail.length == 0 then szef ! Ile(slowo, licznik) // doszlismy do konca to zwracamy wynik licznika
        else { // inaczej lecimy dalej tak dlugo az dojedziemy no konca
          val isPracownik = mapaPracownikow.find(x => x._1 == slowo_tail.head)
          isPracownik match {
            case None => 
              szef ! Ile(slowo, 0)
            case Some(odbiorca) =>
              odbiorca._2 ! Policz(slowo, slowo_tail.tail, szef) 
          }
        }
      
    }
  }


