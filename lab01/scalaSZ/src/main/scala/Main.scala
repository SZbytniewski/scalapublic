@main def mainProg: Unit = {
  println(napis)
}

val napis = "Hello"

@main
def prog2(n: Int): Unit = {
  println(s"Dostalem argument $n")
}


@main
def prog3(imie: String,wiek: Int,czy_gra: Boolean): Unit ={
  val granie = if czy_gra then "gra" else "nie gra"
  println(s"$imie ma $wiek lat i $granie w kule")
}

@main
def prog4(n: Int) : Unit ={
  val rand = scala.util.Random()
  val liczba = rand.nextInt(100)
  if n >= 0 && n <=100 then
    val czy_liczba = if n == liczba then "jest rowna" else if n > liczba then "jest mniejsza" else "jest wieksza"
    println(s"Wylosowano $liczba i $czy_liczba ")
  else
    println("Liczba n nie jest z przedzialu 0-100")
}

@main def prog5(): Unit ={
  if 1<0 then{
    println("1 mniejsze od 0")
  }else if 1==0 then{
    println(" 1 rowne 0")
  }else{
    println("1 wieszke od 0")
  }
}