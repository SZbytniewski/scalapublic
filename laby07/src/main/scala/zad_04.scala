package lab07

def przestaw[A](l: List[A]): List[A] = {
  val even = l.zipWithIndex.collect {
    case (liczba,numer) if(numer % 2 == 0) => liczba
  }
  val odd = l.zipWithIndex.collect {
    case (liczba,numer) if(numer % 2 != 0) => liczba
  }
  even.zip(odd).flatMap(para => List(para._2, para._1)) ++ (if(even.length > odd.length) List(even.last) else Nil)
}

@main def zad_04: Unit = {
  val wynik = przestaw(List(1, 2, 3, 4, 5,9))
  println(wynik)
}
