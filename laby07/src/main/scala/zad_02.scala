package lab07

def indeksy[A](lista: List[A], el: A): Set[Int] = {
  lista.zipWithIndex.filter{
    case (liczba,sn) => liczba == el
  }.map{
    case (_,sn) => sn
  }.toSet
}

@main def zad_02: Unit = {
  val lista = List(1, 2, 1, 1, 5)
  val wynik = indeksy(lista,7)
  print(wynik)
}