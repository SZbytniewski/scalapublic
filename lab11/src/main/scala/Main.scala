
import akka.actor.{ActorSystem, Actor, ActorLogging, ActorRef, Props}

case object Pileczka
case class Graj01(przeciwnik: ActorRef)
case class Graj02(przeciwnik2: ActorRef, maks: Int)
case class Graj03(opsList: List[ActorRef], opCurr: ActorRef)

class Gracz01 extends Actor with ActorLogging {
  def receive: Receive = {
    case Graj01(oponent) =>
      println(s"Gracz $self rozpoczol gre i podal do ${oponent}")
      oponent ! Pileczka
    case Pileczka =>
      val senderRef: ActorRef = sender()
      println(s"Gracz $self odebrał piłke")
      sender() ! Pileczka
  }
}

class Gracz02 extends Actor with ActorLogging {
  def receive: Receive = {
    case Graj02(oponent,max) => {
      if (max == 0){
        println(s"Koniec gry $oponent nie trafi pileczki")
        context.system.terminate()
      }else{
        println(s"Gramy dalej zostalo ${max-1} odbic")
        oponent ! Graj02(self,max-1)
      }
    }
  }
}

class Gracz03 extends Actor with ActorLogging {
  def receive: Receive = {
        case Graj03(opsList, opCurr) => {
            
            print(s"${opsList}")
            
            context.become(przyjecie (if opsList.tail.isEmpty then opCurr else opsList.tail.head ))

            if (!opsList.tail.isEmpty) 
            {
              println(s"rozpoczecie gry dla ${self}")
              context.become(przyjecie(opsList.tail.head))
              opsList.tail.head ! Graj03(opsList.tail, opCurr)
            }
            else 
            {
              println(s"koniec inicjalizacji ${self}")
              context.become(przyjecie(opCurr))
              opCurr ! Pileczka
            }
            
        }
    }
    def przyjecie(opsList: ActorRef): Receive = {
        case Pileczka => {
            //println(s"${sender()} Otrzymalem pileczke ${self}")
            log.info(s"otrzymalem pileczke")
            opsList ! Pileczka
        }
    }
}

def generatorGraczy2(counter: Int, system: ActorSystem, acc: List[ActorRef] = Nil ): List[ActorRef] = {
    val tekst = counter.toString()
    if (counter > 0) {
        generatorGraczy2(counter-1, system, system.actorOf(Props[Gracz03](),tekst) :: acc)
    } 
    else 
        return acc
}

@main def main: Unit = {
  val system = ActorSystem("Hollywood")
    // val player1 = system.actorOf(Props[Gracz03](), "gracz1")
    // val player2 = system.actorOf(Props[Gracz03](), "gracz2")
    // val player3 = system.actorOf(Props[Gracz03](), "gracz3")
    // val lista = List(player1,player2,player3)
    val lista = generatorGraczy2(10,system)
    lista.head ! Graj03(lista,lista.head)
}
