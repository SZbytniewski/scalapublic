package lab05

def isOrdered[A](leq: (A, A) => Boolean)(l: List[A]): Boolean = {
    if(l.length == 1 ){
        true
    }else{
        if (leq(l.head,l.tail.head) == true){
            isOrdered(leq)(l.tail)
        }else{
            false
        }
        
    }
}

@main def zadanie_01: Unit = {
    val lt = (m: Int, n: Int) => m < n
    val lte = (m: Int, n: Int) => m <= n
    val lista = List(1, 2, 2, 5)
  // Program powinien umożliwić „sprawdzenie” działania
  // funkcji „isOrdered”.
    //println(isOrdered(lt)(lista)) // ==> false
    println(isOrdered(lte)(lista)) // ==> true
}

