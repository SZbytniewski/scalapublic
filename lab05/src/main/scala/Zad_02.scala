package lab05

def deStutter[A](list: List[A]): List[A] = {
  list.foldLeft(List.empty[A]) {
    case (acc, x) if acc.nonEmpty && acc.last == x => acc
    case (acc, x) => acc :+ x
}
}

@main def zadanie_02: Unit = {
  // Program powinien umożliwić „sprawdzenie” działania
  // funkcji „deStutter”.
  val l = List(1, 1, 2, 4, 4, 4, 1, 3)
  val wynik = deStutter(l)
  print(wynik)
}
