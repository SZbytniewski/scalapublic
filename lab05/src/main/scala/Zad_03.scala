package lab05

def chessboard[A](l1: List[A], l2: List[A]): String = {
  val pary = l2.map(litery => l1.map(cyfry => s"($cyfry,$litery)").mkString(" ")).mkString("\n")
  pary
  
}

@main def zadanie_03: Unit = {
  // Program powinien umożliwić „sprawdzenie” działania
  // funkcji „szachownica”.
  val lista1 = List(8,7,6,5,4,3,2,1)
  val lista2 = List("a","b","c","d","e","f","g","h")
  val wynik =  chessboard(lista2,lista1)
  print(wynik)
}








// package lab05

// def chessboard: String = {
//   "aqq"
// }

// @main def zadanie_03: Unit = {
//   // Program powinien umożliwić „sprawdzenie” działania
//   // funkcji „szachownica”.
//   val numbers = List(8,7,6,5,4,3,2,1)
//   val litery = List("a","b","c","d","e","f","g","h")
//   val pary = litery.map( x => numbers.map(y => (x,y)))
//   val pary2 = pary.flatMap(y => y)
//   val pary3 = pary2.map(_.toString).grouped(8).map(_.mkString(", ")).mkString("\n")
//   print(pary3)
// }
