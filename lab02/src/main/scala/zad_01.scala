package lab01

def jestPierwsza(n: Int): Boolean = {
    require(n >= 2)

  // „trzy znaki zapytania” oznaczają wartość
  // (jeszcze) „niezdefiniowaną” – wygodne na
  // wstępnym etapie implementacji, kiedy
  // nie wszystkie elementy programu mamy już
  // „w ręku”…

  // Gdyby ktoś chciał wykorzystać funkcję „pomocniczą”
  // to może ją zdefiniować wewnątrz „jestPierwsza”, np.

    def pomocnicza(a: Int, b: Int): Boolean = {
      if (a == b) true
      else if (a % b == 0) false
      else pomocnicza(a,b+1)
    }

    pomocnicza(n,2)
  // „ostatnie napotkane” wyrażenie zwracane jest jako wynik
  // działania funkcji – np.
}

@main def zad_01: Unit = {
  print("Podaj liczbę naturalną: ")
  val liczba = io.StdIn.readInt()
  val wynik = if jestPierwsza(liczba) then "" else " nie"
  println(s"Liczba $liczba$wynik jest liczbą pierwszą")
}
