package lab01

/*
  Funkcja „obramuj” „zdefiniowana” poniżej wykorzystuje dwa parametry
    - „napis” potencjalnie może mieć kilka linijek (patrz przykład)
    - „znak”, z którego należy zbudować ramkę
*/
def obramuj(napis: String, znak: Char): String = {
  // definiujemy funkcję obramowującą
  // val lines = napis.split("\n")
  // var maxWidth = 0
  // for (line <- lines) {
  //   if (line.length > maxWidth) {
  //     maxWidth = line.length
  //   }
  // }

  // val border = znak.toString * (maxWidth + 4)
  // var spaces = ""
  // val linie = for {line <- lines
  //   spaces = " " * (maxWidth - line.length)
  // } yield s"$znak $line$spaces $znak"

  // var pomcona = linie.mkString("\n")
  // s"$border\n$pomcona\n$border"
  val wyrazy = napis.split("\n")
  val maxWidth = for {
    el <- wyrazy
  } yield el.length
  val dlugosc = maxWidth.max
  
  val border = znak.toString * (dlugosc + 4)
  val pomcona = for {
    el <- wyrazy
    odstepy = " " * (dlugosc - el.length)
  }yield s"$znak $el$odstepy $znak"

  val koniec = pomcona.mkString("\n")
  s"$border\n$koniec\n$border"
}

@main def zad_02: Unit = {
  val wynik = obramuj("ala\nma\nkota", '*')
  println(wynik)
  
  /*
    Efektem powino być coś podobnego do:

    ********
    * ala  *
    * ma   *
    * kota *
    ********

  */
}
