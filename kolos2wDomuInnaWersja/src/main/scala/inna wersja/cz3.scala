package kolokwium_2

import scala.io.StdIn.readLine
import scala.concurrent.duration.*


/*
  UWAGA: W sformułowaniu zadania zabrakło jednej wskazówki/informacji:

    Jeśli słowo, o którego liczbę wystąpień pytamy, nie występuje w strukturze
    hierarchii aktorów, to program powinien zwrócić dla niego (tzn. Szef powinien
    wypisać w konsoli) wartość 0.

    Przykładowo, w sytuacji zobrazowanej na rysunku, komunikaty I("al") oraz
    I("alert") powinny zwrócić (w konsoli) odpowiedź 0.
*/

import akka.actor.{ActorSystem, Actor, ActorLogging, ActorRef, Props}

// Metoda nie wymaga zmian. Wczytuje dane z pliku i zwraca listę słów
private def dane: List[String] = {
  import scala.io.Source
  val plik = Source.fromFile("src/main/resources/dane.txt", "UTF-8")
  plik
    .getLines.toList
    .flatMap { linia => linia.split("[^\\p{IsAlphabetic}]+").toList }
}

@main def main: Unit = {
  val system = ActorSystem("sys")
  val szef = system.actorOf(Props[Szef](), "szef")
  implicit val executionContext = system.dispatcher
  dane.foreach(d => szef ! W(d))
  
  //dane.foreach(d => szef ! I(d))
  dane.foreach(d => system.scheduler.scheduleOnce(2.seconds, szef, I(d)))
}