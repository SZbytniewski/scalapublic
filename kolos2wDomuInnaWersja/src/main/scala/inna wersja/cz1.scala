package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

abstract class DoSzefa
case class W(słowo: String) extends DoSzefa
case class I(słowo: String) extends DoSzefa
case class Ile(słowo: String, n: Int) extends DoSzefa

class Szef extends Actor with ActorLogging {
  def receive: Receive = {
    case W(slowo) => {
      val r = scala.util.Random
      val actorName = r.nextInt(1000000).toString
      val newActor = context.actorOf(Props[Pracownik](), actorName)
      val map = Map[Char, ActorRef]()
      context.become(aktorzy(map + (slowo.head -> newActor)))
      newActor ! Wstaw(slowo)
    }
    case I(slowo) => {
      val num = 0
      log.info(s"$slowo występuje $num razy")
    }
    case Ile(slowo, n) => {
      log.info(s"$slowo występuje $n razy")
    }
  }

  def aktorzy(dzieci: Map[Char, ActorRef]): Receive = {
    case W(slowo) => {
      if (slowo.size != 0) {
        val actorFromMap = dzieci.get(slowo.head)       
        actorFromMap match {
          case Some(aktor) => {
            context.become(aktorzy(dzieci))
            aktor ! Wstaw(slowo)
          }
          case None => {
            val r = scala.util.Random
            val actorName = r.nextInt(1000000).toString
            val newActor = context.actorOf(Props[Pracownik](), actorName)
            context.become(aktorzy(dzieci + (slowo.head -> newActor)))
            newActor ! Wstaw(slowo)
          }
        }
      }
    }
    case I(slowo) => {
      val actorFromMap = dzieci.get(slowo.head)
      actorFromMap match {
        case Some(aktor) => {
          aktor ! Policz(slowo, slowo, self)
        }
        case None => {
          val num = 0
          log.info(s"$slowo występuje $num razy")
        }
      }
    }
    case Ile(slowo, n) => {
      log.info(s"$slowo występuje $n razy")
    }
  }
}