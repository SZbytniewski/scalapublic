package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

abstract class DoPracownika
case class Wstaw(słowo: String) extends DoPracownika
case class Policz(słowo: String, fullSlowo: String, szef: ActorRef) extends DoPracownika

class Pracownik extends Actor with ActorLogging {
  def receive: Receive = {
    case Wstaw(slowo) => {
      if (slowo.size != 1) {
        val r = scala.util.Random
        val actorName = r.nextInt(1000000).toString
        val newActor = context.actorOf(Props[Pracownik](), actorName)
        val map: Map[Char, ActorRef] = Map[Char, ActorRef]()
        context.become(literaWSrodku(map + (slowo.head -> newActor)))
        newActor ! Wstaw(slowo.tail)
        
      } else {
        context.become(literaNaKoncu(1, Map[Char, ActorRef]()))
      }
    }
  }

  def literaWSrodku(dzieci: Map[Char, ActorRef]): Receive = {
    case Wstaw(slowo) => {
      if (slowo.size != 1) {
        val actorFromMap = dzieci.get(slowo.head)       
        actorFromMap match {
          case Some(aktor) => {
            context.become(literaWSrodku(dzieci))
            aktor ! Wstaw(slowo.tail)
          }
          case None => {
            val r = scala.util.Random
            val actorName = r.nextInt(1000000).toString
            val newActor = context.actorOf(Props[Pracownik](), actorName)
            context.become(literaWSrodku(dzieci + (slowo.head -> newActor)))
            newActor ! Wstaw(slowo.tail)
          }
        }
      } else {
        context.become(literaNaKoncu(1, dzieci))
      }
    }
    case Policz(słowo, fullSlowo, szef) => {
      if (słowo.size == 1) {
        szef ! Ile(fullSlowo, 0)
      } else {
        val actorFromMap = dzieci.get(słowo.head)       
        actorFromMap match {
          case Some(aktor) => {
            aktor ! Policz(słowo.tail, fullSlowo, szef)
          }
          case None => {
            szef ! Ile(fullSlowo, 0)
          }
        }
      }
    }
  }

  def literaNaKoncu(litery: Int, dzieci: Map[Char, ActorRef]): Receive = {
    case Wstaw(slowo) => {
      if (slowo.size != 1) {
        val actorFromMap = dzieci.get(slowo.head)       
        actorFromMap match {
          case Some(aktor) => {
            context.become(literaNaKoncu(litery, dzieci))
            aktor ! Wstaw(slowo.tail)
          }
          case None => {
            val r = scala.util.Random
            val actorName = r.nextInt(1000000).toString
            val newActor = context.actorOf(Props[Pracownik](), actorName)
            context.become(literaNaKoncu(litery, dzieci + (slowo.head -> newActor)))
            newActor ! Wstaw(slowo.tail)
          }
        }
      } else {
        context.become(literaNaKoncu(litery + 1, dzieci))
      }
    }
    case Policz(słowo, fullSlowo, szef) => {
      if (słowo.size == 1) {
        szef ! Ile(fullSlowo, litery)
      } else {
        val actorFromMap = dzieci.get(słowo.head)       
        actorFromMap match {
          case Some(aktor) => {
            aktor ! Policz(słowo.tail, fullSlowo, szef)
          }
          case None => {
            szef ! Ile(fullSlowo, 0)
          }
        }
      }
    }
  }
}
