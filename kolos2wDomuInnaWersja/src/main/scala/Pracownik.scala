package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props}
import akka.actor.ActorRef

abstract class DoPracownika
case class Wstaw(słowo: String) extends DoPracownika
case class Start(szef: ActorRef) extends  DoPracownika
case class Licz(słowo: String, pełneSłowo: String) extends DoPracownika

class Pracownik extends Actor with ActorLogging {
  def receive: Receive = {
    case Start(szef) => // inicjalizacja pracownika do roboty
      context.become(obsługa(szef, Map[Char, ActorRef](), 0))
    // case msg => log.info(s"Odebrałem wiadomość w stanie receive: ${msg}")
  }

  def obsługa(szef: ActorRef, mapaPracownikow: Map[Char, ActorRef], ilośćSłów: Int): Receive = {
    case Wstaw(słowo) =>
      if słowo.length == 1 then // jak jest ostatnia literka albo slow 1 literowe dodajemy 1 do ilosci taki slow
        context.become(obsługa(szef, mapaPracownikow, ilośćSłów + 1)) 
      else // jak slowo > 1
        mapaPracownikow.get(słowo.tail.head) match // sprawdzamy w mapiePracownikow czy JUZ jest ta literka
          case Some(pracownik) => // mam pracownika, wysylam mu resztę słowa
          //jak jest to robimy nowa itercje tego ex ala i alg mamy wtedy a -> l -> a i g maja rozne sciezki
            pracownik ! Wstaw(słowo.tail)
          case None =>  // nie mam pracownika do tej literki wiec tworzymy nowego
            val nowyPracownik = context.actorOf(Props[Pracownik]())
            nowyPracownik ! Start(szef) // incjalizacja
            nowyPracownik ! Wstaw(słowo.tail) // wstawianie
            context.become(obsługa(szef, mapaPracownikow + (słowo.tail.head -> nowyPracownik), ilośćSłów))

    case Licz(słowo, pełneSłowo) =>
      if słowo.length == 1 then // jak dojdziemy do konca to wysylam odp
        szef ! Ile(pełneSłowo, ilośćSłów) // wysylamy ilosc slow tego tej drogi bo kazde slowo na koncu
      else
        mapaPracownikow.get(słowo.tail.head) match
          case Some(pracownik) =>
            pracownik ! Licz(słowo.tail, pełneSłowo) // przechodzimy z iteracji do iteracji slowa tak dlugo az dojdziemy na koniec
          case None =>  // nie ma takiego słowa
            szef ! Ile(pełneSłowo, 0) 
    // case msg => log.info(s"Odebrałem wiadomość w stanie obsługa: ${msg}")
  }
}
