package lab07

def usuń[A](lista: List[A], k: Int): List[A] = {
  lista.zipWithIndex.filter { case(_,i) => i != k}.map { case (liczba,_) => liczba}
}

@main def zad_01: Unit = {
  val zmienna = List(1,2,3,4,5,6,7,8,9)
  println(usuń(zmienna,1))
}