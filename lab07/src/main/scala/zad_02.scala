package lab07

def indeksy[A](lista: List[A], el: A): Set[Int] = {
  lista.zipWithIndex.filter { case(liczba,_) => liczba == el}.map { case (_,i) => i}.toSet
}

@main def zad_02: Unit = {
    val zmienna = List(1,2,3,1,5,6,1,8,9)
    println(indeksy(zmienna,1))
}
