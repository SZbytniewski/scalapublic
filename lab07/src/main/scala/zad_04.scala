package lab07

def przestaw[A](l: List[A]): List[A] = {
  l.grouped(2).flatMap(_.reverse).toList
}

@main def zad_04: Unit = {
  val lista = List(1, 2, 3, 4, 5)
  print(przestaw(lista))
}
//try with zipwtiht index map and case using them you might be able to do it -zbtyta 11:43
