package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

abstract class DoSzefa
case class W(słowo: String) extends DoSzefa
case class I(słowo: String) extends DoSzefa
case class Ile(słowo: String, n: Int) extends DoSzefa

class Szef extends Actor with ActorLogging {
  def receive: Receive = {
    case W(slowo) =>
      val pracownik = context.actorOf(Props[Pracownik]())
      pracownik ! Wstaw(slowo)
      context.become(receive2(Map(slowo.head -> pracownik)))

    case I(slowo) => 
      log.info(s"${slowo}: 0") 
  }

  def receive2(dzieci: Map[Char, ActorRef]): Receive = {
    case W(slowo) => 
      val odbiorca_some = dzieci.find(x => x._1 == slowo.head)
      odbiorca_some match {
        case None => 
          val pracownik = context.actorOf(Props[Pracownik]())
          pracownik ! Wstaw(slowo)
          context.become(receive2(dzieci + (slowo.head -> pracownik)))
        case Some(odbiorca) =>
          odbiorca._2 ! Wstaw(slowo)
      }
    
    case I(slowo) => 
      val odbiorca_some = dzieci.find(x => x._1 == slowo.head)
      odbiorca_some match {
        case None => 
          log.info(s"${slowo}: 0")
        case Some(odbiorca) =>
          odbiorca._2 ! Policz(slowo, slowo, self) 
      }

    case Ile(slowo, licznik) => 
      log.info(s"${slowo}: ${licznik}")

 }
}
