package kolokwium_2

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

abstract class DoPracownika
case class Wstaw(słowo: String) extends DoPracownika
case class Policz(slowo: String, slowo_tail: String, szef: ActorRef) extends DoPracownika

class Pracownik extends Actor with ActorLogging {
    def receive: Receive = {
      case Wstaw(slowo) =>
        // log.info(s"dostałem ${slowo}")
        if slowo.length == 0 then context.become(litera_na_koncu(Map(), 1))
        else {
          val nowy_pracownik = context.actorOf(Props[Pracownik]())
          nowy_pracownik ! Wstaw(slowo.tail)
          context.become(litera_w_srodku(Map(slowo.head -> nowy_pracownik)))
        }
      
    }

    def litera_w_srodku(dzieci: Map[Char, ActorRef]): Receive = {
      case Wstaw(slowo) =>
        // log.info(s"dostałem ${slowo}")
        if slowo.length == 0 then context.become(litera_na_koncu(dzieci, 1))
        else {  
          val odbiorca_some = dzieci.find(x => x._1 == slowo.head)
          odbiorca_some match {
            case None =>
              val nowy_pracownik = context.actorOf(Props[Pracownik]())
              nowy_pracownik ! Wstaw(slowo.tail)
              context.become(litera_w_srodku(dzieci + (slowo.head -> nowy_pracownik)))
            case Some(odbiorca) => 
              odbiorca._2 ! Wstaw(slowo.tail)
          }
        }

      case Policz(slowo, slowo_tail, szef) =>
        if slowo_tail.length == 0 then {
          szef ! Ile(slowo, 0)
        } 
        else {
          val odbiorca_some = dzieci.find(x => x._1 == slowo_tail.head)
          odbiorca_some match {
            case None => 
              szef ! Ile(slowo, 0)
            case Some(odbiorca) =>
              odbiorca._2 ! Policz(slowo, slowo_tail.tail, szef) 
          }
        }
    }

    def litera_na_koncu(dzieci: Map[Char, ActorRef], licznik: Int): Receive = {
      case Wstaw(slowo) =>
        // log.info(s"dostałem ${slowo}")
        if slowo.length == 0 then {
          context.become(litera_na_koncu(dzieci, licznik + 1))
        } else {  
          val odbiorca_some = dzieci.find(x => x._1 == slowo.head)
          odbiorca_some match {
            case None =>
              val nowy_pracownik = context.actorOf(Props[Pracownik]())
              nowy_pracownik ! Wstaw(slowo.tail)
              context.become(litera_na_koncu(dzieci + (slowo.head -> nowy_pracownik), licznik))
            case Some(odbiorca) => 
              odbiorca._2 ! Wstaw(slowo.tail)
          }
        }

      case Policz(slowo, slowo_tail, szef) =>
        if slowo_tail.length == 0 then szef ! Ile(slowo, licznik)
        else {
          val odbiorca_some = dzieci.find(x => x._1 == slowo_tail.head)
          odbiorca_some match {
            case None => 
              szef ! Ile(slowo, 0)
            case Some(odbiorca) =>
              odbiorca._2 ! Policz(slowo, slowo_tail.tail, szef) 
          }
        }
      
    }
  }


