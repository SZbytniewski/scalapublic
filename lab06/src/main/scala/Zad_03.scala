package lab06

def difficult[A](list: List[A])(len: Int, shift: Int = 1): List[List[A]] = {
  def pomocna(l: List[A], acc: List[List[A]]): List[List[A]] = l match {
    case Nil => acc.reverse
    case _ => 
      val podlista = l.slice(0,len)
      pomocna(l.slice(shift,l.length), podlista :: acc)

  }
  pomocna(list,Nil)
}

@main def zadanie_03: Unit = {
  val ( list, len, shift ) = ( List(1,2,3,4,5), 3, 1 )
  val wynik = difficult(list)(len, shift)
  print(wynik)

}
