package lab06

def freqMax[A](list: List[A]): (Set[A],Int) = {
  ???
}

@main def zadanie_02: Unit = {
  println("Testujemy zadanie 2")
  val lista = List(1,1,2,4,4,3,4,1,3)
  val freqMap = lista.groupBy(identity).map { case (elem, elemList) => (elem, elemList.length) }
  val pomocna = freqMap.values.maxByOption.getOrElse(0)
  val maxFreqElements = freqMap.collect {
    case (elem, freq) if freq == pomocna => elem
  }.toSet
  print(maxFreqElements)
  // print(freqMax(lista))
}
