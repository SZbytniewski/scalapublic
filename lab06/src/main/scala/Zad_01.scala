package lab06

def subseq[A](list: List[A], begIdx: Int, endIdx: Int): List[A] = {
  val pomocniczna = list.take(endIdx)
  pomocniczna.drop(begIdx-1)
}

@main def zadanie_01: Unit = {
  println("Testujemy zadanie 1")
  val lista = List(1,2,3,4,5,6,7,8,9,10)
  print(subseq(lista,3,6))
}
