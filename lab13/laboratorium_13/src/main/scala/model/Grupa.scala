package jp1.akka.lab13.model

import akka.actor.{Actor, ActorRef, ActorLogging}

object Grupa {
  case object Runda
  // Zawodnicy mają wykonać swoje próby – Grupa
  // kolejno (sekwencyjnie) informuje zawodników
  // o konieczności wykonania próby i „oczekuje”
  // na ich wynik (typu Option[Ocena])
  case object Wyniki
  // Polecenie zwrócenia aktualnego rankingu Grupy
  // Oczywiście klasyfikowani są jedynie Zawodnicy,
  // którzy pomyślnie ukończyli swoją próbę
  case class Wynik(ocena: Option[Ocena])
  // Informacja o wyniku Zawodnika (wysyłana przez Zawodnika do Grupy)
  // np. Wynik(Some(Ocena(10, 15, 14)))
  // Jeśli zawodnik nie ukończy próby zwracana jest wartość Wynik(None)
  case object Koniec
  // Grupa kończy rywalizację
}


class Grupa(zawodnicy: List[ActorRef]) extends Actor with ActorLogging {
  
  def receive = receive2(Map())

  def receive2(wyniki:  Map[ActorRef, Option[Ocena]]): Receive = {
    case Organizator.Runda => {    //Kazdy zawodnik poprzez w selection Zawodnik* dostaje wiadmosc Grupa.Runda
      println("dostalem cos")                                 // context.actorSelection("user/organizator/*") wysyla do wszystkich dzieci organizatora
      val selection = context.actorSelection("/user/organizator/Zawodnik*")  // context.actorSelection("user/organizator*") wysyla do wszystkich organizatorw
      selection ! Grupa.Runda
    }
    case Grupa.Wynik(ocena) => {
      if (wyniki.size <= zawodnicy.length-1) { // Jesli Map nie ma tyle co aktorzy to ozncza ze jeszcze nie kazdy dostal oceny
        context.become(receive2(wyniki + (sender() -> ocena))) //i wtedy zmieniamy UPDATUJEMY konteks tak aby map mial ten wynik ktory teraz dostal
        //println(s"${wyniki.size} dupa ${zawodnicy.length}")
        if (wyniki.size == zawodnicy.length-1) { // Jka jest tyle ile ma byc to filtrujemy tych ktorzy otrzmali 0 czyli None
          val wyfiltrowaneWyniki = wyniki.filter((x,y) => y != None)
          val organizator = context.actorSelection("/user/organizator") // odnajdujemy organizatora ktorego to jest grupa
          //println(wyfiltrowaneWyniki)
          organizator ! Organizator.Wyniki(wyfiltrowaneWyniki) // wysylamy mu wynik zawodnikow
          context.stop(self) // zatrzumejmy aktora w tym przypadku zatrzymuje siebie
        }
      }
    }
  }

  

}
