package jp1.akka.lab13.model

import akka.actor.{Actor, ActorRef, ActorLogging, Props}
import akka.actor.PoisonPill
import akka.actor.Kill

val akkaPathAllowedChars = ('a' to 'z').toSet union
  ('A' to 'Z').toSet union
  "-_.*$+:@&=,!~';.)".toSet

object Organizator {
  case object Start
  // rozpoczynamy zawody – losujemy 50 osób, tworzymy z nich zawodników
  // i grupę eliminacyjną
  case object Runda
  // polecenie rozegrania rundy (kwalifikacyjnej bądź finałowej) –  wysyłamy Grupa.Runda
  // do aktualnej grupy
  case object Wyniki
  // polecenie wyświetlenia klasyfikacji dla aktualnej grupy
  case class Wyniki(w: Map[ActorRef, Option[Ocena]])
  // wyniki zwracane przez Grupę
  case object Stop
  // kończymy działanie
}


class Organizator extends Actor with ActorLogging {
  // importujemy komunikaty na które ma reagować Organizator
  import Organizator._

  def receive: Receive = receive2(List())

  def receive2(wynik: List[(ActorRef, Option[Ocena])]): Receive = {
    case Start => {
      // tworzenie 50. osób, opdowiadających im Zawodników
      // oraz Grupy eliminacyjnej
      val zawodnicy = List.fill(10) {
        val o = Utl.osoba()
        context.actorOf(Props(Zawodnik(o)), s"Zawodnik-${o.imie}-${o.nazwisko}" filter akkaPathAllowedChars)
      }
      val grupaEliminacyjna = context.actorOf(Props(Grupa(zawodnicy)),"grupaEliminacyjnaNazwa")
    }
      // ...
    // Obsługa pozostałych komunikatów
    case Runda => {
      if (wynik.isEmpty) { // jesli wynikow nie ma to grupa elimincyjna czlu zawodiny ktorzy zotali stowrzenia na start
                          // i wysylamy kazdemu Organizator.runda (zotanie wyslany do Grupa) 
        val taGrupa = context.actorSelection("/user/organizator/grupaEliminacyjnaNazwa")
        taGrupa ! Runda
      } else { // jak tak to wynikinajgorszych poza top 5 zabiajmy PoisonPillem a reszta trafia do nowej grupy finalistow
              // i graja final TA CZESZ ODPALI SIE TYLKO PO ELIMINACJACH
        wynik.slice(5,10).map((x, y) => x).foreach(x => x ! PoisonPill)
        val wyciagamyAktorow = wynik.slice(0,5).map((x,y) => x)
        //println(wyciagamyAktorow)
        val finalowaGrupa = context.actorOf(Props(Grupa(wyciagamyAktorow)),"grupaFinalowaNazwa")
        //println(finalowaGrupa)
        finalowaGrupa ! Runda
      }
    }
      /*
    o1 > o2 jeśli:
      - suma1 > suma2  (suma = nota1 + nota3 + nota3)
      - suma1 == suma2 oraz o1.nota1 > o2.nota1
      - suma1 == suma2, o1.nota1 == o2.nota1 oraz o1.nota3 > o2.nota3

    Jeśli o1 i o2 mają identyczne wszystkie noty to mamy REMIS

  */

    case Wyniki(listaWynikow) => {
      // val sortedListaWynikow = listaWynikow.toList.sortBy((a,b) => b match {
      //   case Some(Ocena(x,y,z)) => (x+y+z)
      //   case _ => 0 //musi zwrocic wartosc liczbowa do sorta
      // }).reverse.slice(0,21)
      val result = listaWynikow.toList.sortBy { // sortuje liste bo sumie potem x,y,z i potem jest update wynikow
        case (_, Some(Ocena(ocena1, ocena2, ocena3))) => (ocena1 + ocena2 + ocena3, ocena1, ocena2, ocena3)
        case _ => 0
      }(Ordering.by { case (suma: Int,ocena1: Int,ocena2: Int,ocena3: Int) => (suma,ocena1,ocena2,ocena3) }).reverse
      context.become(receive2(result))
      //println(result)
      //println(sortedListaWynikow)
      
    }
    case Wyniki => { //drukuje wynik jak jest jesli nie ma to mowi tam ze nie ma
      if (wynik.isEmpty) then println("czekam na wyniki...") else
      //    {
      //   wynik.foreach((x,y) => println(s"1. ${x} - ${y} - ${y match {
      //     case Some(Ocena(x,y,z)) => x+y+z 
      //     case _ => 0
      //   }}"))
      // }
      {
        def wyswietlanie(lista: List[(ActorRef, Option[Ocena])], i: Int = 1, last: Int = 0): Unit = {
          if (!lista.isEmpty) {
            lista.head match {
              case (name,Some(x,y,z)) => if (last != x+y+z) {
                  println(s"${i}. $name $x - $y - $z = ${x+y+z}")
                  wyswietlanie(lista.tail, i+1, x+y+z)
                }
                else {
                  println(s"${i}. $name $x - $y - $z = ${x+y+z}")
                  wyswietlanie(lista.tail, i, x+y+z)
                }
              case _ => println("")
            }
          }
        }

        wyswietlanie(wynik)
      }
      
    }

    case Stop =>
      log.info("kończymy zawody...")
      context.system.terminate()
  }
}
