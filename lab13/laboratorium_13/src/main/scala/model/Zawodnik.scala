package jp1.akka.lab13.model

import akka.actor.{Actor, ActorLogging}

object Zawodnik {
  case object Próba
  // polecenie wykonania próby (kończy się zwróceniem Wyniku,
  // za pomocą komunikatu Grupa.Wynik)
}

class Zawodnik(o: Osoba) extends Actor with ActorLogging {
  override def preStart(): Unit = {    //preStart co robi aktor jak sie zinicjalizuje   
                                       //preStop wykonuje instrukcje przy wylaczeniu aktora
    println(s"${self.path}")
  }
  def receive: Receive = { //Wysyla spowrotem do sendera swoj wynik jak dostal
    case Grupa.Runda => {
      sender() ! Grupa.Wynik(Utl.ocena())
    }
  }
}
