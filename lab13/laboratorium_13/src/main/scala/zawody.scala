package jp1.akka.lab13

// „Interfejs użytkownika” wymaga pewnych dodatkowych elementów:
import scala.concurrent.ExecutionContext
import scala.util.control.Breaks._
import scala.io.StdIn

import akka.actor.{ActorSystem, Props}

import model.*

@main
def zawody: Unit = {

  val system = ActorSystem("system")
  val organizator = system.actorOf(Props[Organizator](), "organizator")

  // Interfejs „organizatora”:
  implicit val ec: ExecutionContext = ExecutionContext.global

  breakable {  // odpalamy to komenda run
    while (true) {
      StdIn.readLine("polecenie: ") match {
        case "start" =>
          // początek zawodów organizator dostaje znak startu
          organizator ! Organizator.Start
        case "eliminacje" =>
          organizator ! Organizator.Runda
          // polecenie rozegrania rundy eliminacyjnej
        case "finał" =>
          organizator ! Organizator.Runda // funkcja bedzie dostepna dopiero po elimicjach ale dziala tak samo
          // polecenie rozegrania rundy finałowej
          // wymaga zamknięcia Rundy eliminacyjnej i utworzenie
          // Rundy finałowej, zawierającej najlepszych 20.
          // zawodników z Rundy eliminacyjnej
        case "wyniki" =>
          // żądanie rankingów (lub rankingu finałowego)
          organizator ! Organizator.Wyniki
        case "stop" =>
          organizator ! Organizator.Stop // koniec 
          break()
        case _ =>
      }
    }
  }

}