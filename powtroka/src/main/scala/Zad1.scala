import akka.actor.*
import scala.concurrent.duration.*

val rand = new scala.util.Random


//case class Przepytaj( /* wymyśl listę argumentów */ )
case object Przepytaj
case class Rekrutacja(kandydaci: Set[ActorRef])
case class Przyjmij(podzieleniKandydaci: Set[ActorRef])
case object DoWidzenia
case object Przyjety
case object Dziekuje
case object Odp
case class Najlepszy(kandydat: (ActorRef, Int))

class Pracodawca extends Actor { 
    def receive: Receive = {
        case Rekrutacja(setKandydatow) => {
            val iloscEgzaminatorw = setKandydatow.size/3
            //println(iloscEgzaminatorw)
            val egzaminatorzy = (for (i <- 1 to iloscEgzaminatorw) yield context.actorOf(Props[Egzaminator](),s"egzaminator-$i")).toList
            val podzialKandydatow = setKandydatow.grouped(3).toList
            //println(podzialKandydatow)
            for (i <- 0 to iloscEgzaminatorw-1) yield { egzaminatorzy(i) ! Przyjmij(podzialKandydatow(i)) }
            context.become(receive2(Map(), iloscEgzaminatorw))

        }
    }
    def receive2(mapNajlepszych: Map[ActorRef, Int], wielkoscListyEgz: Int): Receive = {
        case Najlepszy(kandydat) => {
            if (mapNajlepszych.size < wielkoscListyEgz - 1) {
                //println(mapNajlepszych)
                context.become(receive2(mapNajlepszych + kandydat, wielkoscListyEgz))
            } else {
                val wynik = mapNajlepszych + kandydat
                val najlepsiKandydaci0 = wynik.toList.sortBy((x,y) => y).reverse
                val najlepsiKandydaci = najlepsiKandydaci0.filter((x,y) => y == najlepsiKandydaci0.head._2)
                //println(najlepsiKandydaci)
                if (najlepsiKandydaci.length > 1) {
                    val index = rand.between(0,najlepsiKandydaci.length)
                    najlepsiKandydaci(index)._1 ! Przyjety
                    najlepsiKandydaci.filter((x,y) => x != najlepsiKandydaci(index)._1).foreach((x,y) => x ! DoWidzenia)
                } else {
                    najlepsiKandydaci.head._1 ! Przyjety
                    println(s"${najlepsiKandydaci.head._1} => ${najlepsiKandydaci.head._2}")
                    //println(najlepsiKandydaci0.tail)
                    najlepsiKandydaci0.tail.foreach((x,y) => x ! DoWidzenia)
                }
                context.become(receive2(mapNajlepszych + kandydat, wielkoscListyEgz))
            }
        }
        case Dziekuje => {
            println("TARCZA SZMATO!!!")
            context.system.terminate()
        }
    }
    

 }


class Egzaminator extends Actor with ActorLogging { 
    def receive: Receive = {
        case Przyjmij(currKandydaci) => {
            currKandydaci.toList.foreach(element => element ! Przepytaj)
            context.become(kontaktZKandydatami(currKandydaci, Map()))
        }
    }
    def kontaktZKandydatami(obecniKandydaci: Set[ActorRef], wynikiKandydatow: Map[ActorRef, Int]): Receive = {
        case Odp => {
            if (obecniKandydaci.size-1 > wynikiKandydatow.size) {
                val wynikEgz = rand.between(0,101)
                context.become(kontaktZKandydatami(obecniKandydaci, wynikiKandydatow + (sender() -> wynikEgz)))
            } else {
                val wynikEgz = rand.between(0,101)
                val noweGowno = wynikiKandydatow + (sender() -> wynikEgz)
                val najlepszyKandydat = noweGowno.toList.sortBy((x,y) => y).reverse.head
                //log.info(s"$najlepszyKandydat")
                //println(wynikiKandydatow)
                noweGowno.toList.sortBy((x,y) => y).reverse.tail.foreach((x,y) => x ! DoWidzenia)
                //noweGowno.toList.sortBy((x,y) => y).reverse.tail.foreach((x,y) => println(s"CHUJ$x"))
                context.actorSelection("/user/OnlyPracodawca") ! Najlepszy(najlepszyKandydat)
            }
            
        }
    }
 }

class Kandydat extends Actor with ActorLogging {
    def receive: Receive = {
        case Przepytaj => {
            sender() ! Odp
        }
        case DoWidzenia => {
            log.info("zabijam sie")
            self ! PoisonPill
        }
        case Przyjety => {
            log.info("a ja sie nie zabijam")
            sender() ! Dziekuje
            context.stop(self)
        }
    }
 }



@main def praca: Unit = {

    val system = ActorSystem("Praca")
    val pracodawca = system.actorOf(Props[Pracodawca](),"OnlyPracodawca")
    val setPracownikow = (for (i <- 1 to 35) yield system.actorOf(Props[Kandydat](),s"kandydat-$i")).toSet
    pracodawca ! Rekrutacja(setPracownikow)

}