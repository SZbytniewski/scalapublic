import akka.actor.*
object Samochód {
  case object Dalej
}
class Samochód extends Actor with ActorLogging {

  def receive: Receive = {
    case Kierowca.PolaczWlasciciela => {
      context.become(receive2(sender()))
    }
  }

  def receive2(wlasciciel: ActorRef): Receive = {
    case Samochód.Dalej => {
      val failRate = rand.between(0,100)
      if (failRate < 15) {
        wlasciciel ! Kierowca.ReakcjaAuta(None)
      }
      else {
        val speed = rand.between(1,201)
        wlasciciel ! Kierowca.ReakcjaAuta(Some(speed))
      }
    }
  }


}
