import akka.actor.*

object Organizator {
  case class UstawMaksCyk(maksCyk: Int) {
    assert(maksCyk > 0)
  }
  case object Cyk
  case class PrzejechanaTrasa(liczbaKm: Int)
}

val liczbakierowcow = 30

class Organizator extends Actor with ActorLogging {
  import Organizator.*
  def receive = {
    case UstawMaksCyk(mc) =>
      println(s"liczba cyknięć do wykonania: $mc")
      val warsztat = context.actorOf(Props[Warsztat](),"WarsztatJedyny")
      val kierowcy = (for (i <- 1 to liczbakierowcow) yield context.actorOf(Props[Kierowca](),s"kierowca-$i")).toList
      kierowcy.foreach(element => element ! Kierowca.PrzygotujAuto)
      context.become(poInicjalizacji(mc,Map()))
    case _ => // inne pomijamy
  }

  def poInicjalizacji(maksCyk: Int, mapaKierowcow: Map[ActorRef, Int]): Receive = {
    case Cyk =>
      if (maksCyk > 1) {
        val warsztat = context.actorSelection("/user/organizator/WarsztatJedyny") 
        warsztat ! Cyk
        val kierowcy = context.actorSelection("/user/organizator/kierowca*") // USEFUL (wysyla do wszystkich)
        kierowcy ! Cyk
        context.become(poInicjalizacji(maksCyk-1,mapaKierowcow))
      }
      else {
        context.actorSelection("/user/organizator/kierowca*") ! Kierowca.PodajTrasę
      }
    case PrzejechanaTrasa(trasa) => {
      context.become(poInicjalizacji(maksCyk, mapaKierowcow + (sender() -> trasa)))
      if (mapaKierowcow.size == liczbakierowcow) {
        
        val wynik = mapaKierowcow.toList.sortBy((x,y) => y).reverse
        wynik.foreach(el => println(el))
        context.become(koniec)
        context.system.terminate()
        
      }
    }
  }
  def koniec: Receive = {
    case _ => 
  }
}

