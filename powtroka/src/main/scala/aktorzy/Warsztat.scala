import akka.actor.*
object Warsztat {
  case object Cyk
  case class Awaria(auto: ActorRef)
}
class Warsztat extends Actor with ActorLogging {

  def receive: Receive = naprawa(Map())

  def naprawa(mapKierowcow: Map[ActorRef, Int]): Receive = {
    case Organizator.Cyk => {
      val temp = mapKierowcow.map((x,y) => (x,y-1))
      val czyzero = temp.filter((x,y) => y == 0).foreach((x,y) => {
        val losowanko = rand.between(0,100)
        if (losowanko < 80) {
          x ! Kierowca.WynikNaprawy(Some(x))
        }
        else {
          x ! Kierowca.WynikNaprawy(None)
        }
      })
      //println(s"temp: $temp")
      context.become(naprawa(temp.filter((x,y) => y != 0)))
    }
    case Warsztat.Awaria(wrak) => {
      val iletickow = rand.between(1,6)
      context.become(naprawa(mapKierowcow + (wrak -> iletickow)))
    }
  }
}
