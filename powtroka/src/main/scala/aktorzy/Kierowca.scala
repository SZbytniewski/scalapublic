import akka.actor.*
import scala.compiletime.ops.float

object Kierowca {
  case object Cyk
  case object PrzygotujAuto
  case class ReakcjaAuta(ov: Option[Int])
  case object PodajTrasę
  case class WynikNaprawy(efekt: Option[ActorRef])
  case object PolaczWlasciciela
}
class Kierowca extends Actor with ActorLogging {
  import Kierowca.*
  //val dt = 15 // w minutach
  def receive: Receive = {
    case PrzygotujAuto => {
      val auto = context.actorOf(Props[Samochód]())
      auto ! PolaczWlasciciela
      context.become(kierowanieAutem(auto,1))
    }
  }
  def kierowanieAutem(samochod: ActorRef, totalDroga: Int): Receive = {
    case Organizator.Cyk => {
      //println("kierowaniecyk")
      samochod ! Samochód.Dalej
    }
    case ReakcjaAuta(currSpeed) => {
      currSpeed match {
        case Some(value) => {
          val dt: Double = 15.0/60.0
          val temp: Double = totalDroga + (value * dt)
          //println(s"temp: $temp,dt: $dt,:currSpeed: $value")
          //println(s"${self} ${totalDroga}")
          //println(value)
          context.become(kierowanieAutem(samochod, temp.toInt))
        }
        case None => {
          context.actorSelection("/user/organizator/WarsztatJedyny") ! Warsztat.Awaria(samochod)
          context.become(samochodNaprawa(samochod, totalDroga))
        }
      }
    }
    case PodajTrasę => {
      sender() ! Organizator.PrzejechanaTrasa(totalDroga)
    }
  }
  def samochodNaprawa(samochod: ActorRef, totalDroga: Int): Receive = {
    case Kierowca.WynikNaprawy(wynikNaprawy) => {
      wynikNaprawy match {
        case Some(value) => context.become(kierowanieAutem(samochod, totalDroga))
        case None => context.become(rozwalonySamochod(samochod,totalDroga))
      }
    }
    case PodajTrasę => {
      sender() ! Organizator.PrzejechanaTrasa(totalDroga)
    }
    case _ =>
  }
  def rozwalonySamochod(samochod: ActorRef, totalDroga: Int): Receive = {
    case PodajTrasę => {
      sender() ! Organizator.PrzejechanaTrasa(totalDroga)
    }
    case _ => 
  }
}
