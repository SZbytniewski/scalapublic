import akka.actor.{ActorSystem, Actor, ActorLogging, ActorRef, Props}
import akka.actor.PoisonPill

case object Zaglosuj

case class Sprawdz(glos: Option[Int])

case class Sprawdz2(glos: Option[Int])

case class Glos(nrListy: Int)

case class Przyjmij(setObywateli: Set[ActorRef])

case object Zaglosuj2

case object Koniec

//case class Wyslij(listaObywateli: List[ActorRef])
val rand = new scala.util.Random

class PKW extends Actor { 

  def receive: Receive = {
    case Przyjmij(setObywateli) => {
      val iloscKomisji = setObywateli.size/5
      val komisje = (for ( i <- 1 to iloscKomisji ) yield context.actorOf(Props[Komisja](),s"komisja-$i")).toList
      val podzialObywateli = setObywateli.grouped(iloscKomisji).toList
      //println(podzialObywateli)
      for ( i <- 0 to iloscKomisji-1) yield komisje(i) ! Przyjmij(podzialObywateli(i))
      context.become(zliczanieGlosow(Map((1 -> 0),(2 -> 0),(3 -> 0), (4 -> 0), (5 -> 0)),0))
    }
  }

  def zliczanieGlosow(glosy: Map[Int,Int], doKonca: Int): Receive = {
    case Glos(nrListy) => {
      context.become(zliczanieGlosow(glosy + (nrListy -> (glosy(nrListy)+1)),doKonca))
    }
    case Koniec => 
      if (doKonca == 4) {
        val zwyciezca = glosy.toList.sortBy((x,y) => y).reverse.head
        println(s"kandydat -> ${zwyciezca} <- liczba glosow")
       
      }
      context.become(zliczanieGlosow(glosy,doKonca+1))
  }

 }

class Komisja extends Actor with ActorLogging {
  def receive = receive2(0) 

  def receive2(iloscOdp: Int): Receive = {
    case Przyjmij(setObywateli) => {
      setObywateli.toList.foreach(element => element ! Zaglosuj)
    }
    case Sprawdz(glos) => {
      if (iloscOdp == 4) context.actorSelection("/user/PKW") ! Koniec
      glos match {
        case Some(value) => { 
          context.actorSelection("/user/PKW") ! Glos(value)
          context.become(receive2(iloscOdp+1))
        }
        case _ => {
          sender() ! Zaglosuj2
        }
      }
    }
    case Sprawdz2(glos) => {
      if (iloscOdp == 4) context.actorSelection("/user/PKW") ! Koniec
      glos match {
        case Some(value) => { 
          context.actorSelection("/user/PKW") ! Glos(value)
          context.become(receive2(iloscOdp+1))
        }
        case _ => {
          context.become(receive2(iloscOdp+1))
        }
      }
    }
  }
  
 }

class Obywatel extends Actor { 

  def receive: Receive = {
    case Zaglosuj => {
      val isVote = rand.between(0,2)
      val vote = rand.between(1,6)
      if (isVote == 0) sender() ! Sprawdz(None) else sender() ! Sprawdz(Some(vote))
    }
    case Zaglosuj2 => {
      val isVote = rand.between(0,2)
      val vote = rand.between(1,6)
      if (isVote == 0) sender() ! Sprawdz2(None) else sender() ! Sprawdz2(Some(vote))
    }
  }
 }

@main def wybory: Unit = {
  val system = ActorSystem("Wybory")
  val setObywateli = (for ( i <- 1 to 25 ) yield system.actorOf(Props[Obywatel](),s"Obywatel-$i")).toSet
  val pkw = system.actorOf(Props[PKW](),"PKW")
  pkw ! Przyjmij(setObywateli)
   

}