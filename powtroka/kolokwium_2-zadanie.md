# Zadanie

## Inicjalizacja

 W programie głównym zdefiniowany został już `Planista` o nazwie `pantaRhei` oraz `Organizator`, który po uruchomieniu (utworzeniu) powinien stworzyć aktora typu `Warsztat` oraz pewną liczbę `Kierowców` i wysłać do wszystkich `Kierowców` komunikat
 ```scala
 // Kierowca.scala
 case object PrzygotujAuto
 ```

w efekcie którego każdy z nich powinien uruchomić (stworzyć) swój `Samochód`.


## Wyścig

Po inicjalizacji, `Planista` (`pantaRhei`) cyklicznie wysyła komunikaty `Cyk` do `Organizatora`, który z kolei wysyła analogiczny komunikat do `Warsztatu` i wszystkich `Kierowców`. `Warsztat` aktualizuje czas naprawy przebywających w nim `Samochodów` (szczegóły opisane w sekcji [Naprawa samochodu](#naprawa)). Każdy `Kierowca` „wciska pedał gazu” (wysyłając do swojego `Samochodu` komunikat `Dalej`), na co `Samochód` reaguje w sposób opisany w następnym podpunkcie i zwraca `Kierowcy` odpowiedź typu `ReakcjaAuta`:

```scala
// Kierowca.scala
case class ReakcjaAuta(ov: Option[Int]) // ov może więc mieć dwie postaci:

Some(v) // „v” – to prędkość, z jaką samochód będzie jechał aż do następnego „cyk”
None    // nastąpiła awaria samochodu
```

Jeśli komunikat zawiera wartość pedkości `v`, `Kierowca` „z góry” aktualizuje przejechany dystans korzystając z danych `v` oraz `dt` – wyjaśnienie roli tego parametru znajdziesz w sekcji [Uwagi](#uwagi).

## Zmiana prędkości lub awaria

`Samochód` __losując__ prędkość ma szansę ulec awarii (może to zajść ze stałym prawdopodobieństwem `15%`). `Samochód`, który ma awarię, informuje o niej `Kierowcę` (wysyłając odpowiedź `ReakcjaAuta(None)`), który następnie zgłasza to, wysyłając komunikat typu
```scala
// Warsztat.scala
case class Awaria(auto: ActorRef)
```
 do `Warsztatu`.

## Naprawa samochodu <a name="naprawa"></a>

 Kiedy `Samochód` jest w `Warsztacie`, `Kierowca` powinien przyjąć  „inną tożsamość”, w której pomija sygnały `Cyk` od `Organizatora`, gdyż (przynajmniej chwilowo) nie ma możliwości kontynuowania wyścigu. Oczywiście musi w tym czasie być gotowy na ewentualny sygnał zakończenia wyścigu i zwrócenie informacji o pokonanym dystansie.

`Warsztat` __losowo__ ustala czy samochód uda się naprawić (z prawdopodobieństwem sukcesu `80%`). Wynik naprawy zwracany będzie w postaci komunikatu typu:
```scala
// Kierowca.scala
case class WynikNaprawy(efekt: Option[ActorRef])
```

Jeśli __naprawa jest niemożliwa__ `Warsztat` wysyła do `Kierowcy` komunikat:
```scala
WynikNaprawy(None)
```
W takim przypadku `Samochód` nie kontynuuje już wyścigu, a wynik końcowy, który `Kierowca` zwróci na koniec wyścigu będzie zawierał informację o dystansie jaki pokonał do momentu wystąpienia awarii.

Jeśli `Samochód` (powiedzmy `autoRef`) __da się naprawić__, to naprawa trwa – w __losowy__ sposób – `1-6` cykli („cyknięć”). Po tym czasie `Warsztat` odsyła do `Kierowcy` komunikat:
```scala
WynikNaprawy(Some(autoRef))
```
a on ponownie zaczyna reagować na sygnały „cyk” i „wciska pedał gazu” kontynuując wyścig.

__Uwaga__! Ponieważ `Warsztat` jest jeden, a `Kierowców` – wielu więc `Warsztat` musi pamiętać (korzystając z odpowiedniej tożsamości) który `Kierowca` przysłał który `Samochód` (definiując wspomnianą tożsamość skorzystaj z odpowiedniej kolekcji i wartości `sender()`).

## Wyniki końcowe

Załóżmy, że „czas” trwania wyścigi wyznaczany jest „całkowitą liczbą cyknięć” `TotalCyk`, która zdefiniowana jest w programie głównym. Po `TotalCyk` „cyknięciach” wysyłanych przez `pantaRhei` i zliczonych przez `Organizatora`, ten ostatni wysyła komunikat:
```scala
// Kierowca.scala
case object PodajTrasę
```
do wszystkich `Kierowców`, którzy odpowiadają mu, podając łączną długość trasy, jaką udało im się przejechać, używając komunikatów postaci:
```scala
// Organizator.scala
case class PrzejechanaTrasa(liczbaKm: Int)
```
Po zebraniu wszystkich odpowiedzi, `Organizator` publikuje (wpisując na ekranie) wyniki (powinien wziąć pod uwagę ewentualne pozycje __ex aequo__) i kończy działanie całego systemu (używając `context.system.terminate()`).

# Uwagi <a name="uwagi"></a>

Poniżej opisane zostały wartości, które aktorzy poszczególnych typów wykorzystują bądź udostępniają do wykorzystania innym.

- `Organizator`

    - __dt__ – stały „krok czasowy” wyrażony w minutach, którym `Organizator` inicjalizuje `Kierowców`
    - całkowita liczba „cyknieć” – kiedy osiągnie wartość `MaksCyk` `Organizator` kończy wyścig, zbiera i publikuje jego wyniki oraz zamyka system aktorów.


- `Kierowca`

    - __s__ – droga przebyta do tej pory (początkowo równa `0` i aktualizowana zgodnie ze wzorem `s = s + dt*v` w reakcji na każdy sygnał („cyk”) „zegara” – trzeba pamiętać, że prędkość wyrażamy w __km/h__, a wartość __dt__ wyrażona jest w _minutach_)

    - __dt__ – stały „przyrost czasu” (wartość otrzymana od `Organizatora` w fazie Inicjalizacji) – wyrażony w _minutach_.

    - __v__ – ostatnio zarejestrowana prędkość (otrzymana od samochodu, w reakcji na wciśnięcie gazu) – wyrażona w __km/h__

- `Samochód`

  - __v__ – aktualna prędkość (aktualizowana __losowo__ z przedziału [0, 200] __km/h__, po otrzymaniu od `Kierowcy` sygnału „wciśnięcia gazu” `Dalej`). Jeśli `Samochód` nie uległ awarii (może ona zajść z prawdopodobieństwem `15%`) to wylosowaną wartość `v` odsyłamy jako nową wartość prędkości do `Kierowcy`, wykorzystując komunikat `ReakcjaAuta(Some(v))`.

<div>
<img src="kolokwium_2-schemat.png" alt="drawing" style="width:80%; align: center"/>
</div>
