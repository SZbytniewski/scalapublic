import akka.actor._
import scala.concurrent.duration._
import scala.util.Random

val r = scala.util.Random

/*
  W konfiguracji projektu wykorzystana została wtyczka
  sbt-revolver. W związku z tym uruchamiamy program poleceniem

    reStart

  a zatrzymujemy pisząc (mimo przesuwających się komunikatów)

     reStop

  i naciskając klawisz ENTER. Jeśli czynności powyższe
  już wykonywaliśmy to możemy też przywołać poprzednie
  polecenia używając strzałek góra/dół na klawiaturze.
*/

// Przykład wykorzystania Planisty (Scheduler)
object Zad1 {

  case class StartWojny()
  case class RozkazAtaku()
  case class RozkazDlaZamku()
  case class Zacznij(listaLucznikow: List[ActorRef], zamekWroga: ActorRef)
  case class Przygotowanie(zamekWroga: ActorRef)
  case class Strzal()
  case class Zabojstwo()

  class Lucznik extends Actor {
    def receive: Receive = { // otrzymuje i zmina konetks na read z zamkiem worga
      case Przygotowanie(zamekWroga: ActorRef) =>
        context.become(ready(zamekWroga))
    }

    def ready(zamekWroga: ActorRef): Receive = {
      case Strzal => 
        zamekWroga ! Strzal     
    }
  }

  class Zamek extends Actor {
    def receive: Receive = {
      case Zacznij(listaLucznikow: List[ActorRef], zamekWroga: ActorRef) => //Gotowosc zacznij -> ready
        context.become(ready(zamekWroga, listaLucznikow, listaLucznikow.size))
        println(s"${self.path.name}: jestem gotowy!")
    }

    def ready(zamekWroga: ActorRef, listaLucznikow: List[ActorRef], ilosc: Int): Receive = {
      case RozkazAtaku => // kazdy lucznik strzela po zmienieni konteksu na oczekiwanie na wyniki
        context.become(waitingForResults(zamekWroga, listaLucznikow, listaLucznikow.size))
        println(s"${self.path.name}: atakuje!")
        listaLucznikow.foreach(x => x ! Strzal)
    }

    def waitingForResults(zamekWroga: ActorRef, listaLucznikow: List[ActorRef], ilosc: Int): Receive = {
      case Strzal => // losujemy sznase na trafienie 
        val szansaNaTrafienie = ilosc.toDouble/200
        val rand = r.nextFloat
        println(s"rand: ${rand}, szansa: ${szansaNaTrafienie}")
        if(szansaNaTrafienie > rand){ // jesli udalo sie to umiera lucznik z zamku wroga
          val nowaListaLucznikow = listaLucznikow.tail
          context.become(waitingForResults(zamekWroga, nowaListaLucznikow, ilosc-1)) // update archerow
          listaLucznikow.head ! PoisonPill
        }
        println(s"${self.path.name}: ilosc Lucznikow - ${ilosc}")
        if(ilosc-1 == -1){ // jak ktos wygra
          println(s"Zamek Wroga: ${zamekWroga.path.name} - Wygrywa!")
          context.system.terminate()
        }
      case RozkazAtaku =>
        context.become(ready(zamekWroga, listaLucznikow, listaLucznikow.size)) // zmienay na ready i wyslamy do sibie Rozka ataku
        self ! RozkazAtaku
    }

  }

  class SilaWyzsza extends Actor {
    def receive: Receive = {
      case StartWojny => //rozpoczynmay wojne przez tworzeni dla obu stron 100 archerow i tworzenia zamkow kazdy z custom id 
                          //potem dla kazdego lucznika wysylamy wiadomosc o przygotowaniu sie  a potem dla kazdego zamku rozpoczecie 
                          // i zmieniamy konteks na fighting
        println("Start Wojny!")
        val lucznicy1 = (for {
            i <- 0 until 100
          } yield context.actorOf(Props[Lucznik](), s"Lucznik-${i}")).toList
        val lucznicy2 = (for {
            i <- 100 until 200
          } yield context.actorOf(Props[Lucznik](), s"Lucznik-${i}")).toList 

        val castle1 = context.actorOf(Props[Zamek](), "Castle-1")
        val castle2 = context.actorOf(Props[Zamek](), "Castle-2")

        lucznicy1.foreach(x => x ! Przygotowanie(castle2))
        lucznicy2.foreach(x => x ! Przygotowanie(castle1))

        castle1 ! Zacznij(lucznicy1, castle2)
        castle2 ! Zacznij(lucznicy2, castle1)

        context.become(fighting(castle1, castle2))
    }

    def fighting(castle1: ActorRef, castle2: ActorRef): Receive = {
      case RozkazAtaku => // oba zamki atakuja
        castle1 ! RozkazAtaku
        castle2 ! RozkazAtaku
    }
    //def waiting()
  }

  object TickActor {
    val Tick = "tick"
  }
  class TickActor extends Actor {
    import TickActor._
    def receive: Receive = {
      case Tick => //rozpoczynam wojne poprzez tworzeni silywyzszej i wysyalmy do niej object StartWojny i zmienamy konteks na warStart
        val sila = context.actorOf(Props[SilaWyzsza](), "silaWyzsza")
        sila ! StartWojny
        context.become(warStart(sila))
        println("Program rozpoczyna wojne!")
    }

    def warStart(sila: ActorRef): Receive = {
      case Tick => //teraz walczymy
        println("Tick Wojenny:")
        sila ! RozkazAtaku
    }
  }

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("system")
    import system.dispatcher

    val tickActor = system.actorOf(Props[TickActor](), "defender")

    val ticker = system.scheduler.scheduleWithFixedDelay(
      Duration.Zero,
      1000.milliseconds, //co sekunde wysyalmy to tickAcotr wysylamy ticka
      tickActor,
      TickActor.Tick
    )



    // system.terminate()

  }
}