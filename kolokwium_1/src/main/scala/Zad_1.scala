//@annotation.tailrec
def wystąpienia(arg: List[Int]): List[(Int,Int)] = {
  val odp = arg.groupBy(identity).map{
    case (elem,ilosc) => (elem, ilosc.length)
  }.toList
  odp
}

/*
  Poprawność rozwiązania należy testować (z poziomu SBT) poleceniem:
  testOnly Test1
*/


@main def zad_1: Unit = {
  // „program główny” ma znaczenie czysto pomocnicze
  val arg = List(1,2,3,3,2,1)
  val wyn = wystąpienia(arg)
  println(wyn)
}
