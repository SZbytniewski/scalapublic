package lab09

sealed trait MyList[+A]
case object Empty extends MyList[Nothing]
case class Cons[+A](head: A, tail: MyList[A]) extends MyList[A]

object MyList {

  def head[A](list: MyList[A]): A = list match {
    case Cons(h, tl) => h
    case _ => throw IllegalArgumentException("Head of an empty MyList")
  }

  // wynik: MyList-a zawierająca wszystkie elementy poza pierwszym
  def tail[A](list: MyList[A]): MyList[A] = list match {
    case Cons(head, tail) => tail
    case _ => throw IllegalArgumentException("Tail of an emplty MyList")
  }

  // wynik: długość MyList-y będącej argumentem
  def length[A](list: MyList[A], acc: Int = 0): Int = list match {
    case Cons(head, tail) => length(tail, acc+1)
    case Empty => acc
  }

  // wynik: MyList-a zawierająca wszystkie elementy poz n początkowymi
  def drop[A](list: MyList[A], n: Int): MyList[A] = (list, n) match {
    case (_,0) => list
    case (Cons(_,tail),_) => drop(tail,n-1)
    case _ => Empty
  }

  // wynik: „odwrócony” argument
  def reverse[A](list: MyList[A]): MyList[A] = {
    def pomocna(lst: MyList[A], acc: MyList[A]): MyList[A] = lst match{
      case Cons(head, tail) => pomocna(tail, Cons(head,acc))
      case _ => acc
    }
    pomocna(list,Empty)
  }
  

  // wynik: argument po odrzuceniu początkowych elementów spełniających p
  def dropWhile[A](l: MyList[A])(p: A => Boolean): MyList[A] = l match {
    case Cons(head, tail) if p(head) => dropWhile(tail)(p)
    case _ => l 
  }

  // wynik: połączone MyList-y list1 oraz list2
  def append[A](list1: MyList[A], list2: MyList[A]): MyList[A] = list1 match {
    case Empty => list2
    case Cons(head, tail) => Cons(head, append(tail,list2))
  }
  
  
  

  // wynik: MyList-a składająca się ze wszystkich alementów argumentu, poza ostatnim
  def allButLast[A](list: MyList[A]): MyList[A] = list match {
    case Empty => Empty
    case Cons(_,Empty) => Empty
    case Cons(head, tail) => Cons(head,allButLast(tail))
  }
  

}

@main def listy: Unit = {
  val l1 = Cons(1, Cons(2, Cons(3, Empty)))
  val l2 = Cons(4, Cons(5, Cons(6, Empty)))

  val res = MyList.head(l1)
  val res2 = MyList.tail(l1)
  val res3 = MyList.length(l1)
  val res4 = MyList.drop(l1,2)
  val res5 = MyList.reverse(l1)
  val res6 = MyList.dropWhile(l1)(_ != 2)
  val res7 = MyList.append(l1 , l2)
  val res8 = MyList.allButLast(l1)
  println(s"MyList.head($l1) == $res")
  println(s"MyList.tail($l1) == $res2")
  println(res3)
  println(res4)
  println(res5)
  println(res6)
  println(res7)
  println(res8)
  // println(MyList.head(Empty)) // spowoduje „wyjątek”
}

