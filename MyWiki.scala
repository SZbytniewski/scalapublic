//context
context.actorOf // tworzy nam nowego aktora ktry jest dzieckiem aktora w ktorym jest tworzyony
context.become // zmienia stan recive aktora na inny 
context.parent // zwraca do rodzica aktora 
context.actorSelection // pozwala nam wybrac aktora do kotrego sie odwulujemy przez wybranie drogi do niego


//system
system.actorOf // tworzy nowego aktora
system.scheduler.scheduleWithFixedDelay //
system.dispatcher // 

//Random
rand.nextFloat // losow wynbiera od 0.0 do 1.0

PoisonPill // zabija aktora

//lista
cos match {
    case Some(value) =>

    case None
}

//mapy
val emptyMap: Map[String, Int] = Map.empty
val map = emptyMap + ("apple" -> 3) + ("banana" -> 5) + ("orange" -> 2)
val containsBanana = map.contains("banana")
val multipliedMap = map.map { case (key, value) => (key, value * 2) }
val filteredMap = map.filter { case (_, value) => value > 4 }
//listy
val emptyList = List.empty[Int]
val list = List(1, 2, 3, 4, 5)
val updatedList = 0 :: list
val updatedList = list :+ 6
val combinedList = list ++ List(6, 7, 8)
val element = list(2) // Retrieves the element at index 2 (zero-based)
val updatedList = list.updated(2, 10) // Replaces the element at index 2 with 10
val containsThree = list.contains(3)
val length = list.length
val sortedList = list.sorted



._1 ._2
.map
.filter
.flatMap
.foreach

